<!--
    SPDX-License-Identifier: CC0-1.0
    SPDX-FileCopyrightText: 2019 Harald Sitter <sitter@kde.org>
-->

Ever had drkonqi not produce a useful backtrace or was getting in the way of you actually getting to gdb?
Then this will help! ... On Linux.

This is a UI on top of systemd-coredump to integrate into a graphical desktop.

This tool monitors coredumpd reports and issues notifications when detecting a crash.
The notification is equipped with a link to fire up Konsole with `coredumctl gdb` ready to go.

Due to the nature of how core dumping work this works best when drkonqi is disabled so crashes
as quickly as possible result in a core dump.

Sitting on top of coredumpd makes this a really handy tool for developers
because it is faster to get to gdb than with drkonqi AND it is also producing
more reliable backtraces as it relies on the kernel dumping the core instead of
trying to attach to the running process (which has a bunch of complications due to how
SEGV works on POSIX threads).
On the flip side it lacks bug reporting facilities of drkonqi.

# Install

- install systemd-coredump
- install kde-coredump (installs autostart desktop file so it starts with your session)
- to disable drkonqi it's easiest to add `KDE_DEBUG=1` to /etc/environment

# With Drkonqi

You can also use this with drkonqi in which case a crash will still bring up drkonqi
but then also shoot off the dump notification once you quit drkonqi. While this
seems silly, the advantage is that you get notifications for any application crashing,
regardless of it being KCrash-enabled or not while also still able to file bugs via
drkonqi.
