# SPDX-License-Identifier: BSD-3-Clause
# SPDX-FileCopyrightText: 2019 Harald Sitter <sitter@kde.org>

find_program(konsole_EXE konsole)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(konsole
    FOUND_VAR
        konsole_FOUND
    REQUIRED_VARS
        konsole_EXE
)
