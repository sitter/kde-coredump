/*
    SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
    SPDX-FileCopyrightText: 2019-2020 Harald Sitter <sitter@kde.org>
*/

#include <QApplication>
#include <QDebug>
#include <QFile>
#include <QLibraryInfo>
#include <QMetaProperty>
#include <QPointer>
#include <QProcess>
#include <QScopeGuard>
#include <QSessionManager>
#include <QSocketNotifier>
#include <QStandardPaths>

#include <KLocalizedString>
#include <KNotification>

#include <sys/resource.h>
#include <systemd/sd-journal.h>

#include <sys/types.h>
#include <unistd.h>

class Coredump : public QObject
{
    Q_OBJECT
    // Only modeling fields I need for now.
    // All properties must be the toLower name of the journal property without COREDUMP_ prefix
    Q_PROPERTY(pid_t pid MEMBER pid)
    // NB: coredump has UID and OWNER_UID, from glancing over the code the difference is that the former is passed in
    // from the core_pattern and the latter is obtained from the pid (supposedly via procfs). The former is used
    // for chowning of cores and the like, the latter is only recorded as metadata.
    Q_PROPERTY(uid_t uid MEMBER uid)
    Q_PROPERTY(QString exe MEMBER exe)
    Q_PROPERTY(QString filename MEMBER filename)
public:
    typedef QPointer<Coredump> Ptr;

    explicit Coredump(QObject *parent = nullptr)
        : QObject(parent)
    {
        qRegisterMetaType<uid_t>("uid_t");
        qRegisterMetaType<gid_t>("gid_t");
        qRegisterMetaType<rlim_t>("rlim_t");
        qRegisterMetaType<pid_t>("pid_t");
    }

    uid_t uid = 0;
    gid_t gid = 0;
    rlim_t rlimit = 0;
    int session = -1;
    uid_t owner_uid = 0;
    QString slice;
    QString cgroup;
    QString proc_limits; // would need extra parsing
    QString root;
    int signal = -1;
    QString signal_name;
    QString proc_cgroup; // extra parsing
    QString proc_mountinfo; // extra parsing
    pid_t pid = -1;
    QString comm;
    QString exe;
    QString cmdline;
    QString open_fds; // extra parsing
    QString proc_status; // extra parsing
    QString proc_maps; // extra parsing
    QString cwd;
    QString environ; // extra parsing
    QString timestamp;
    QString filename; // core dump file if available

    QString message; // simple trace in the MESSAGE

    bool valid = false; // only set after parsing
};

class CoredumpWatcher : public QObject
{
    Q_OBJECT
public:
    explicit CoredumpWatcher(QObject *parent = nullptr)
        : QObject(parent)
    {
        if (sd_journal_open(&context, 0) != 0) {
            qWarning() << "Failed to open context";
            return;
        }

        // Ideally we should get some identifying marker for the last
        // entry we saw and periodically sync that to a file to then
        // pick up where we left off last. Crashes happening outside
        // the running time of this process are ignored the way we
        // currently always seek to the tail.
        if (sd_journal_seek_tail(context) != 0) {
            qWarning() << "Failed to go to tail";
            return;
        }
        // It seems that seek_tail actually causes some weird
        // off-by-one issue. Make sure we go back to the last
        // entry. I think what happens is that if we call next
        // after the already are on the tail we'd end up at the
        // beginning again.
        (void)sd_journal_previous(context);

        if (sd_journal_add_match(context, "SYSLOG_IDENTIFIER=systemd-coredump", 0) != 0) {
            qWarning() << "Failed to install match";
            return;
        }

        const int fd = sd_journal_get_fd(context);
        if (fd < 0) {
            qWarning() << "Failed to get listening socket";
            return;
        }
        notifier = new QSocketNotifier(fd, QSocketNotifier::Read);
        connect(notifier, &QSocketNotifier::activated, this, [this] {
            Q_ASSERT(notifier);
            if (sd_journal_process(context) != SD_JOURNAL_APPEND) {
                return;
            }
            processLog();
        });

        valid = true;
    }

signals:
    void newCoredump(Coredump::Ptr coredump);

private:
    void processLog()
    {
        if (sd_journal_next(context) <= 0) {
            // Read pointer isn't advancing for whatever reason
            return;
        }

        Coredump::Ptr dump(new Coredump(this));

        const void *data;
        size_t length;
        SD_JOURNAL_FOREACH_DATA(context, data, length)
        {
            // size_t is uint, QBA uses int, make sure we don't overflow the int!
            int dataSize = static_cast<int>(length);
            Q_ASSERT(dataSize >= 0);
            Q_ASSERT(static_cast<quint64>(dataSize) == length);

            QByteArray entry(static_cast<const char *>(data), dataSize);
            const int offset = entry.indexOf('=');
            if (offset < 0) {
                continue;
            }

            const QByteArray key = entry.left(offset).replace("COREDUMP_", "").toLower();
            const QByteArray value = entry.mid(offset + 1);
            auto metaObject = dump->metaObject();
            const int propertyIndex = metaObject->indexOfProperty(key);
            if (propertyIndex < 0) {
                continue; // property not modelled
            }
            dump->setProperty(key, value);

            // object has at least one property so it's valid
            dump->valid = true;
        }

        if (dump->valid) {
            emit newCoredump(dump);
        }
    }

    sd_journal *context = nullptr;
    QSocketNotifier *notifier = nullptr;
    bool valid = false;
};

int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    app.setApplicationName("kde-coredump");

    // Disable session management, we have no use for it and it actually
    // means that lingering processes may get restarted on login for no
    // good reason.
    QGuiApplication::setFallbackSessionManagementEnabled(false);
    auto disableSessionManagement = [](QSessionManager &sm) {
        sm.setRestartHint(QSessionManager::RestartNever);
    };
    QObject::connect(&app, &QGuiApplication::commitDataRequest, disableSessionManagement);
    QObject::connect(&app, &QGuiApplication::saveStateRequest, disableSessionManagement);

    CoredumpWatcher w;
    QObject::connect(&w, &CoredumpWatcher::newCoredump, &w, [&](Coredump::Ptr dump) {
        qDebug() << dump->valid << dump->exe << dump->pid << dump->filename;

        auto notification = new KNotification("applicationcrash");
        notification->setTitle(i18nc("@title of notification", "He's dead, Jim"));
        notification->setText(i18nc("notification text %1 is an exectuable path, %2 is a numeric process id", "%1 [%2]", dump->exe, dump->pid));
        notification->setActions({i18nc("notification action starting gdb for debugging", "gdb")});

        const auto pid = dump->pid;
        const auto uid = dump->uid;
        QObject::connect(notification, static_cast<void (KNotification::*)(unsigned int)>(&KNotification::activated), notification, [pid, uid]() {
            QStringList args{QStringLiteral("--nofork"), QStringLiteral("-e"), QStringLiteral("coredumpctl"), QStringLiteral("gdb"), QString::number(pid)};
            // if the crash isn't from the current user then route it through pkexec
            if (uid != getuid()) {
                args = QStringList{QStringLiteral("--nofork"), QStringLiteral("-e"), QStringLiteral("pkexec coredumpctl gdb %1").arg(pid)};
            }
            QProcess::startDetached(QStringLiteral("konsole"), args);
        });

        notification->setFlags(KNotification::DefaultEvent | KNotification::SkipGrouping);
        notification->sendEvent();
    });
    return app.exec();
}

#include "main.moc"
